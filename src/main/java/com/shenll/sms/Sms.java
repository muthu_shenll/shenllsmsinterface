package com.shenll.sms;

import java.util.List;

public interface Sms {
    void initialize(String... options) throws Exception;

    void prepare(String message, String from, List<String> to) throws Exception;

    boolean send();
}
